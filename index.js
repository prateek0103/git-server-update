const express = require('express');
const app = express();

app.post('/', (req, res, next) => {
  require('child_process').exec('git pull', (error, stdout, stderr) => {
    if (error) {
      console.error(`exec error: ${error}`);
      return;
    }
    console.log(`stdout: ${stdout}`);
    console.log(`stderr: ${stderr}`);
    require('child_process').exec('pm2 restart 0', (error, stdout, stderr) => {
      if (error) {
        console.error(`exec error: ${error}`);
        return;
      }
      console.log(`stdout: ${stdout}`);
      console.log(`stderr: ${stderr}`);
    });
    res.send('SUCCESS');
  });
});

app.listen(3001);